using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoviesDb.Model.Domain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MoviesDb.Model
{
	public class MovieDbContext : DbContext
	{
		public DbSet<Movie> Movies { get; set; }
		public DbSet<Character> Characters { get; set; }
		public DbSet<Franchise> Franchises { get; set; }

		public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			/*
			 * Structure
			 *  - Model creation
			 *  - The seeding data
			 *  - Seeding the data
			 */

			//
			// Model creation
			//
			// Most of the model is created using the EFCore5's socalled "convensions".
			// The only "missing" part, during the seeding below, is the mapping between
			// the movies and the characters.  Creating it here overrides the (otherwise
			// automatical) generation of the table.
			//

			// Create the Character-Movie mapping table
			// NOTE: Seeding is moved to the bottom to keep the structure (see the top of the method)
			EntityTypeBuilder<Dictionary<string, object>> moviesAndCharactersEntity = null; // initial assignment: avoids compiler warning

			modelBuilder.Entity<Character>()
				.HasMany(c => c.Movies)
				.WithMany(m => m.Characters)
				.UsingEntity<Dictionary<string, object>>(
					"CharacterMovie", //Entity Name that would have been used if seeding (and this table definition) was generated automatically
					l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
					r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
					tab =>
					{
						tab.HasKey("MovieId", "CharacterId");
						moviesAndCharactersEntity = tab; // Actual assignment; used to delay seeding data - for code structure.
					}
				);  // returns EntityTypeBuilder<Character>

			// moviesAndCharactersEntity is now initialised to the correct entity.

			//
			// The seeding data
			//
			#region The seeding data
			Franchise[] franchises = new[] {
				new Franchise
				{
					Id = 1,
					Name = "Wizarding World",
					Description = "Fantasy universe centered around the Harry Potter and Fantastic Beasts books and movie series.",
				},

				new Franchise
				{
					Id = 2,
					Name = "Narnia",
					Description = "Fantacy series based on the Narnia Chronicles book series authored by C S Lewis.",
				},

				new Franchise
				{
					Id = 3,
					Name = "Peter Rabbit",
					Description = "Live-action/computer animated films based on the stories of Peter Rabbit created by Beatrix Potter.",
				},
			};

			Character[] characters = new[] {
				new Character {
					Id = 1,
					FullName = "Severus Snape",
					Alias = "The Half-Blood Prince",
					Gender = EGender.Male,
					Picture = "https://harrypotter.fandom.com/wiki/Severus_Snape?file=Severus_Snape.jpg",
				},

				new Character {
					Id = 2,
					FullName = "Eustace Clarence Scrubb",
					Alias = null,
					Gender = EGender.Male,
					Picture = "https://static.wikia.nocookie.net/narnia/images/7/76/Eustace_dawntreader.png",
				},

				new Character {
					Id = 3,
					FullName = "Peter Rabbit",
					Alias = null,
					Gender = EGender.Male,
					Picture = "https://www.peterrabbit-movie.com/images/character.png",
				},

				new Character {
					Id = 4,
					FullName = "Hermione Jean Granger",
					Alias = null,
					Gender = EGender.Female,
					Picture = "https://static.wikia.nocookie.net/hpmor/images/f/fe/Hermione_granger_2.jpg",
				},

				new Character
				{
					Id = 5,
					FullName = "Lucy Pevensie",
					Alias = null,
					Gender = EGender.Female,
					Picture = "https://en.wikipedia.org/wiki/Lucy_Pevensie#/media/File:Lucy_Pevensie.jpg",
				}
			};

			var movies = new[] {
				new {
					Id = 1,
					Title = "Harry Potter and the Philosopher's Stone",
					Director = "Chris Joseph Colombus",
					ReleaseYear = 2001,
					Genre = "Fantasy",
					Picture = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)#/media/File:Harry_Potter_and_the_Philosopher's_Stone_banner.jpg",
					Trailer = "https://www.youtube.com/watch?v=mNgwNXKBEW0",
					FranchiseId = 1,
				},

				new {
					Id = 2,
					Title = "Peter Rabbit 2: The Runaway",
					Director = "Will Gluck",
					ReleaseYear = 2001,
					Genre = "Fantasy",
					Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/e/ee/Peter_Rabbit_2_-_RT_poster.png/220px-Peter_Rabbit_2_-_RT_poster.png",
					Trailer = "https://www.youtube.com/watch?v=PWBcqCz7l_c",
					FranchiseId = 3,
				},

				new {
					Id = 3,
					Title = "The Voyage of the Dawn Treader",
					Director = "Michael Apted",
					ReleaseYear = 2010,
					Genre = "Fantasy",
					Picture = "https://en.wikipedia.org/wiki/The_Chronicles_of_Narnia:_The_Voyage_of_the_Dawn_Treader#/media/File:The_Voyage_of_the_Dawn_Treader_poster.jpg",
					Trailer = "https://www.youtube.com/watch?v=hrJQDPpIK6I",
					FranchiseId = 2,
				},

				new {
					Id = 4,
					Title = "Prince Caspian",
					Director = "Andrew Adamson",
					ReleaseYear = 2005,
					Genre = "Fantasy",
					Picture = "https://en.wikipedia.org/wiki/File:Principe_Caspain_poster.jpg",
					Trailer = "https://www.youtube.com/watch?v=oZRZ-2et2uY",
					FranchiseId = 2,
				},

			};

			var moviesAndCharactersMapping = new[] {
				new { MovieId = 1, CharacterId = 1 }, // HP1, Snape
				new { MovieId = 2, CharacterId = 3 }, // The Runaway, Peter Rabbit
				new { MovieId = 3, CharacterId = 2 }, // Voyage if the Dawn Treader, Eustace
				new { MovieId = 1, CharacterId = 4 }, // HP1, Hermione
				new { MovieId = 4, CharacterId = 5 }, // Prince Caspian, Lucy
				new { MovieId = 4, CharacterId = 2 }, // Prince Caspian, Eustace
				new { MovieId = 3, CharacterId = 5 }, // Voyage if the Dawn Treader, Lucy
			};
			#endregion

			//
			// Seeding the data
			//
			modelBuilder.Entity<Franchise>().HasData(franchises);

			modelBuilder.Entity<Character>().HasData(characters);

			modelBuilder.Entity<Movie>().HasData(movies);

			moviesAndCharactersEntity.HasData(moviesAndCharactersMapping);
		}
	}
}
