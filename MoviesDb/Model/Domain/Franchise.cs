using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoviesDb.Model.Domain
{
	/// <summary>
	/// Defines a franchise profile
	/// </summary>
	[Table("Franchise")]
	public class Franchise
	{
		/// <summary>
		/// Identification of the movie franchise profile
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Name of the franchise.
		/// </summary>
		/// <remarks>Max 50 characters.</remarks>
		/// <example>The Harry Potter movies belongs to the 'Wizarding World' franchise.</example>
		[Required]
		[MaxLength(50)]
		public string Name { get; set; }

		/// <summary>
		/// A short description of the franchise.
		/// </summary>
		/// <remarks>Max 200 characters.</remarks>
		[Required]
		[MaxLength(200)]
		public string Description { get; set; }

		// Navigation property towards the Movie entity
		public ICollection<Movie> Movies { get; set; }

	}
}
