using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoviesDb.Model.Domain
{
	/// <summary>
	/// The profile of a movie.
	/// </summary>
	[Table("Movie")]
	public class Movie
	{
		/// <summary>
		/// Identification of the movie profile
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Movie title
		/// </summary>
		/// <remarks>Max 100 characters.</remarks>
		[Required]
		[MaxLength(100)]
		public string Title { get; set; }

		/// <summary>
		/// A comma separated list of genres the movie belongs to
		/// </summary>
		/// <remarks>Max 50 characters.</remarks>
		[Required]
		[MaxLength(50)]
		public string Genre { get; set; }

		/// <summary>
		/// The year the movie was released.
		/// </summary>
		public int ReleaseYear { get; set; }

		/// <summary>
		/// The name of the director of the movie.
		/// </summary>
		/// <remarks>Max 50 characters.</remarks>
		[Required]
		[MaxLength(50)]
		public string Director { get; set; }

		/// <summary>
		/// The URL of a picture that represents the movie.  Typically a movie poster.
		/// </summary>
		/// <remarks>Max 200 characters.</remarks>
		[Url]
		[Required]
		[MaxLength(200)]
		public string Picture { get; set; }

		/// <summary>
		/// The URL to a trailer for the movie.
		/// </summary>
		/// <remarks>Max 200 characters.</remarks>
		[Url]
		[Required]
		[MaxLength(200)]
		public string Trailer { get; set; }

		/// <summary>
		/// Navigation property towards the Character entity.
		/// </summary>
		public ICollection<Character> Characters { get; set; }

		/// <summary>
		/// Navigation property towards the Franchise entity.
		/// </summary>
		public Franchise Franchise { get; set; }

	}
}
