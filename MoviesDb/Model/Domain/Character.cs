using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoviesDb.Model.Domain
{
	/// <summary>
	/// Defines the profile of a character.
	/// </summary>
	[Table("Character")]
	public class Character
	{
		/// <summary>
		/// Character profile identification
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Full name for the character.
		/// </summary>
		/// <remarks>Example: Albus Percival Wulfric Brian Dumbledore</remarks>
		[Required]
		[MaxLength(100)]
		public string FullName { get; set; }

		/// <summary>
		/// An alias for the character.
		/// </summary>
		/// <remarks>A null value is allowed.</remarks>
		/// <example>Severus Snape called himself "the Half Blood Prince"</example>
		[MaxLength(100)]
#nullable enable
		public string? Alias { get; set; }
#nullable restore

		/// <summary>
		/// The character's gender
		/// </summary>
		/// <remarks>Becomes an integer in a Microsoft SQL Server
		/// Database.</remarks>
		public EGender Gender { get; set; }

		/// <summary>
		/// Url to a picture of the character.
		/// </summary>
		/// <remarks>Limited to 200 characters.</remarks>
		[Url]
		[Required]
		[MaxLength(200)]
		public string Picture { get; set; }

		/// <summary>
		/// Navigation property towards the Movie entity
		/// </summary>
		public ICollection<Movie> Movies { get; set; }
	}
}
