namespace MoviesDb.Model.Domain
{
	/// <summary>
	/// Enumeration used to identify the gender of something/someone
	/// </summary>
	public enum EGender
	{
		Male,
		Female,
	}
}