using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesDb.Model.DTO
{
	public class CharacterDTO
	{
		/// <summary>
		/// Character ID
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Full name of the character
		/// </summary>
		/// <remarks>See <see cref="Domain.Character.FullName"/></remarks>
		public string FullName { get; set; }

		/// <summary>
		/// The character's alias (if one exist)
		/// </summary>
		/// <remarks>See <see cref="Domain.Character.Alias"/></remarks>
#nullable enable
		public string? Alias { get; set; }
#nullable restore
		/// <summary>
		/// The gender of the character (male of female)
		/// </summary>
		/// <remarks>See <see cref="Domain.Character.Gender"/></remarks>
		public string Gender { get; set; }

		/// <summary>
		/// URL to a picture of the character (or the actor playing the character).
		/// </summary>
		/// <remarks>See <see cref="Domain.Character.Picture"/></remarks>
		[Url]
		public string Picture { get; set; }

		/// <summary>
		/// List of movie identifirers
		/// </summary>
		/// <remarks>See also <seealso cref="Domain.Character.Movies"/></remarks>
		public List<int> Movies { get; set; }
	}
}
