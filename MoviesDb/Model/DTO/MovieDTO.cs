using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesDb.Model.DTO
{
	public class MovieDTO
	{
		/// <summary>
		/// Movie identifier
		/// </summary>
		/// <remarks>See <see cref="Domain.Movie.Id"/></remarks>
		public int Id { get; set; }

		/// <summary>
		/// Movie Title
		/// </summary>
		/// <remarks>See <see cref="Domain.Movie.Title"/></remarks>
		public string Title { get; set; }

		/// <summary>
		/// Comma separated list of genres.
		/// </summary>
		// <remarks>See <see cref="Domain.Movie.Genre"/></remarks>
		public string Genre { get; set; }

		/// <summary>
		/// The year the movie was released.
		/// </summary>
		/// <remarks>See <see cref="Domain.Movie.ReleaseYear"/></remarks>
		public int ReleaseYear { get; set; }

		/// <summary>
		/// The director of the movie.
		/// </summary>
		// <remarks>See <see cref="Domain.Movie.Director"/></remarks>
		public string Director { get; set; }

		/// <summary>
		/// URL to a poster for the movie.
		/// </summary>
		/// <remarks>See <see cref="Domain.Movie.Picture"/></remarks>
		[Url]
		public string Picture { get; set; }

		/// <summary>
		/// URL to a trailer for the movie.
		/// </summary>
		// <remarks>See <see cref="Domain.Movie.Trailer"/></remarks>
		[Url]
		public string Trailer { get; set; }

		/// <summary>
		/// Identifies which characters that belongs to this movie.
		/// </summary>
		/// <remarks>See also <seealso cref="Domain.Movie.Characters"/></remarks>
		public ICollection<int> Characters { get; set; }

		/// <summary>
		/// Identifies which franchise this movie belongs to.
		/// </summary>
		/// <remarks>See also <seealso cref="Domain.Movie.Franchise"/></remarks>
		public int Franchise { get; set; }

	}
}