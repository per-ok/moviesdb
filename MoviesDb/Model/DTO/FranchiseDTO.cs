using System.Collections.Generic;

namespace MoviesDb.Model.DTO
{
	public class FranchiseDTO
	{
		/// <summary>
		/// Franchise identifier.
		/// </summary>
		// <remarks>See <see cref="Domain.Franchise.Id"/></remarks>
		public int Id { get; set; }

		/// <summary>
		/// Name of the franchise.
		/// </summary>
		// <remarks>See <see cref="Domain.Franchise.Name"/></remarks>
		public string Name { get; set; }

		/// <summary>
		/// Description of the francise.
		/// </summary>
		// <remarks>See <see cref="Domain.Franchise.Description"/></remarks>
		public string Description { get; set; }

		/// <summary>
		/// Identifiers of movies belonging to the franchise.
		/// </summary>
		// <remarks>See <see cref="Domain.Franchise.Movies"/></remarks>
		public IEnumerable<int> Movies { get; set; }
	}
}
