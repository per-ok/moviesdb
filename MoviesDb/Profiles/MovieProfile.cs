using AutoMapper;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Linq;

namespace MoviesDb.Profiles
{
	/// <summary>
	/// Mapping profile
	/// </summary>
	public class MovieProfile : Profile
	{
		public MovieProfile()
		{
			CreateMap<Movie, MovieDTO>()
				.ForMember(dto => dto.Characters,
					opt => opt.MapFrom(m => m.Characters.Select(c => c.Id)))
				.ForMember(dto => dto.Franchise,
					opt => opt.MapFrom(m => m.Franchise.Id));

			CreateMap<MovieDTO, Movie>()
				.ForMember(m => m.Characters, opt => opt.Ignore())
				.ForMember(m => m.Franchise, opt => opt.Ignore());
		}
	}
}
