using AutoMapper;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Linq;

namespace MoviesDb.Profiles
{
	public class CharacterProfile : Profile
	{
		/// <summary>
		/// Mapping profile
		/// </summary>
		public CharacterProfile()
		{
			CreateMap<Character, CharacterDTO>()
				.ForMember(dto => dto.Movies,
					opt => opt.MapFrom(c => c.Movies.Select(m => m.Id)))
				.ForMember(dto => dto.Gender,
					opt => opt.MapFrom(c => c.Gender.ToString()))
				.ReverseMap();
		}
	}
}
