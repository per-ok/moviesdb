using AutoMapper;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Linq;

namespace MoviesDb.Profiles
{
	public class FranchiseProfile : Profile
	{
		/// <summary>
		/// Mapping profile
		/// </summary>
		public FranchiseProfile()
		{
			CreateMap<Franchise, FranchiseDTO>()
				.ForMember(dto => dto.Movies,
					opt => opt.MapFrom(f => f.Movies.Select(m => m.Id)))
				.ReverseMap();
		}
	}
}
