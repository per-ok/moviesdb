using System;
using System.Reflection;

namespace MoviesDb
{
	public static class ObjectCopying
	{
		/// <summary>
		/// Makes a shallow copy of one object into another object.  No clone is made.
		/// Copies public properties and fields including cross copying (properties to fields and fields to properties).
		/// The name and type must be the same.
		/// </summary>
		/// <exception cref="ArgumentNullException"/>
		/// <param name="dest">The object to be copied to.</param>
		/// <param name="source">The object to be copied from.</param>
		public static void ShallowCopy(object dest, object source)
		{
			if (source == null)
				throw new ArgumentNullException("source", "The parameter is null.");
			if (dest == null)
				throw new ArgumentNullException("dest", "The parameter is null.");

			if (object.ReferenceEquals(source, dest))
				return; // the objects are the same
			var from_type = source.GetType();
			var to_type = dest.GetType();

			foreach (PropertyInfo from_property in from_type.GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				if (from_property.GetGetMethod() == null)
					continue;
				PropertyInfo to_property = to_type.GetProperty(from_property.Name, BindingFlags.Instance | BindingFlags.Public);
				if (to_property != null && to_property.PropertyType.IsAssignableFrom(from_property.PropertyType) && to_property.GetSetMethod() != null)
				{
					to_property.SetValue(dest, from_property.GetValue(source));
					continue;
				}
				FieldInfo to_field = to_type.GetField(from_property.Name, BindingFlags.Instance | BindingFlags.Public);
				if (to_field != null && to_field.FieldType.IsAssignableFrom(from_property.PropertyType))
				{
					to_field.SetValue(dest, from_property.GetValue(source));
				}
			}

			foreach (FieldInfo from_field in from_type.GetFields(BindingFlags.Instance | BindingFlags.Public))
			{
				PropertyInfo to_property = to_type.GetProperty(from_field.Name, BindingFlags.Instance | BindingFlags.Public);
				if (to_property != null && to_property.PropertyType.IsAssignableFrom(from_field.FieldType) && to_property.GetSetMethod() != null)
				{
					to_property.SetValue(dest, from_field.GetValue(source));
					continue;
				}
				FieldInfo to_field = to_type.GetField(from_field.Name, BindingFlags.Instance | BindingFlags.Public);
				if (to_field != null && to_field.FieldType.IsAssignableFrom(from_field.FieldType))
					to_field.SetValue(dest, from_field.GetValue(source));
			}
		}

	}
}
