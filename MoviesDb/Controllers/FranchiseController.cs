using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb.Model;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MoviesDb.Controllers
{
	/// <inheritdoc />>
	public class FranchiseController : MovieDbCrudControllerBase<FranchiseDTO>
	{
		/// <inheritdoc />
		public FranchiseController(MovieDbContext context, IMapper mapper) : base(context, mapper)
		{
		}

		#region CRUD

		#region Create
		/// <inheritdoc />
		override public async Task<ActionResult<FranchiseDTO>> Post([FromBody] FranchiseDTO franchiseDTO)
		{
			try
			{
				franchiseDTO.Id = 0;
				Franchise franchise = await MapFromDTO(franchiseDTO);

				_context.Franchises.Add(franchise);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get),
					new { id = franchise.Id },
					_mapper.Map<FranchiseDTO>(franchise));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error creating new item.");
			}
		}

		#endregion

		#region Read
		/// <inheritdoc />
		override public async Task<ActionResult<IEnumerable<FranchiseDTO>>> Get()
		{
			try
			{
				return _mapper.Map<List<FranchiseDTO>>(
					await _context.Franchises
						.Include(f => f.Movies)
						.ToListAsync()
				);
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}

		/// <inheritdoc />
		override public async Task<ActionResult<FranchiseDTO>> Get(int id)
		{
			try
			{
				var franchise = _mapper.Map<FranchiseDTO>(
					await _context.Franchises
					.Include(f => f.Movies)
					.SingleOrDefaultAsync(f => f.Id == id));

				if (franchise == null)
					return NotFound();

				return franchise;
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}
		#endregion

		#region Update
		/// <inheritdoc />
		override public async Task<ActionResult<FranchiseDTO>> Put(int id, [FromBody] FranchiseDTO dto)
		{
			try
			{
				if (id != dto.Id)
					return BadRequest("Mismatching IDs");

				Franchise franchise = await _context.Franchises
					.Include(f => f.Movies)
					.SingleOrDefaultAsync(f => f.Id == id);

				if (franchise == null)
					return NotFound($"Item {id} was not found.");

				// To use the Update() method, we cannot simply overwrite
				// the old character with a new one.  The object and the
				// navigation properties be the same instance as the one
				// retrieved from the database.
				//
				// The ShallowCopy method copies all elements from the source
				// object to the destination object.  This replaces (in this
				// case) the original Movies property by the new one provided
				// by MapFromDTO().

				// Preserving the Movies object instance (not the content)
				franchise.Movies.Clear();
				var tmp = await MapFromDTO(dto);
				tmp.Movies.ToList().ForEach(m => franchise.Movies.Add(m));
				tmp.Movies = franchise.Movies;
				// Preserving the franchise object instance
				ObjectCopying.ShallowCopy(franchise, tmp);

				_context.Franchises.Update(franchise);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get),
					new { id = franchise.Id }, _mapper.Map<FranchiseDTO>(franchise));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error updating item.");
			}
		}
		#endregion

		#region Delete
		/// <inheritdoc />
		override public async Task<ActionResult<FranchiseDTO>> Delete(int id)
		{
			try
			{
				var franchise = _context.Franchises
					.Include(f => f.Movies)
					.SingleOrDefault(f => f.Id == id);
				if (franchise == null)
					return NotFound($"Item {id} was not found.");

				_context.Franchises.Remove(franchise);
				await _context.SaveChangesAsync();

				return _mapper.Map<FranchiseDTO>(franchise);
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error while deleting item.");
			}
		}
		#endregion

		#endregion

		/// <summary>
		/// Maps from a FranchiseDTO object to a Franchise object.
		/// </summary>
		/// <param name="franchiseDTO"> a FranchiseDTO object</param>
		/// <returns>a Franchise object.</returns>
		private async Task<Franchise> MapFromDTO(FranchiseDTO franchiseDTO)
		{
			IEnumerable<int> movieIds = franchiseDTO.Movies;
			franchiseDTO.Movies = null;

			Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);

			franchise.Movies = await _context.Movies
				.Where(m => movieIds.Contains(m.Id)).ToListAsync();
			return franchise;
		}
	}
}
