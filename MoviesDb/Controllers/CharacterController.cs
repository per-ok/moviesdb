using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb.Model;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MoviesDb.Controllers
{
	/// <inheritdoc />
	public class CharacterController : MovieDbCrudControllerBase<Model.DTO.CharacterDTO>
	{
		/// <inheritdoc />
		public CharacterController(MovieDbContext context, IMapper mapper) : base(context, mapper)
		{
		}

		#region CRUD

		#region Create
		/// <inheritdoc />
		public async override Task<ActionResult<CharacterDTO>> Post([FromBody] CharacterDTO dto)
		{
			try
			{
				dto.Id = 0;
				Character character = await MapFromDTO(dto);

				_context.Characters.Add(character);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get), new { id = character.Id },
					_mapper.Map<CharacterDTO>(character));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error creating new item.");
			}
		}
		#endregion

		#region Read
		/// <inheritdoc />
		public async override Task<ActionResult<IEnumerable<CharacterDTO>>> Get()
		{
			try
			{
				return _mapper.Map<List<CharacterDTO>>(
					await _context.Characters
					.Include(c => c.Movies)
					.ToListAsync());
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}

		/// <inheritdoc />
		public async override Task<ActionResult<CharacterDTO>> Get(int id)
		{
			try
			{
				CharacterDTO character = _mapper.Map<CharacterDTO>(
					await _context.Characters
					.Include(c => c.Movies)
					.SingleOrDefaultAsync(c => c.Id == id));

				if (character == null)
					return NotFound();

				return character;
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}
		#endregion

		#region Update
		/// <inheritdoc />
		public override async Task<ActionResult<CharacterDTO>> Put(int id, [FromBody] CharacterDTO dto)
		{
			try
			{
				if (id != dto.Id)
					return BadRequest("Mismatching IDs");

				Character character = await _context.Characters
					.Include(c => c.Movies)
					.SingleOrDefaultAsync(c => c.Id == id);

				if (character == null)
					return NotFound($"Item {id} was not found.");

				// To use the Update() method, we cannot simply overwrite
				// the old character with a new one.  The object and the
				// navigation properties be the same instance as the one
				// retrieved from the database.
				//
				// The ShallowCopy method copies all elements from the source
				// object to the destination object.  This replaces (in this
				// case) the original Movies property by the new one provided
				// by MapFromDTO().

				// Preserving the Movies object instance (not the content)
				character.Movies.Clear();
				var tmp = await MapFromDTO(dto);
				tmp.Movies.ToList().ForEach(m => character.Movies.Add(m));
				tmp.Movies = character.Movies;
				// Preserving the character object instance
				ObjectCopying.ShallowCopy(character, tmp);

				_context.Characters.Update(character);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get),
					new { id = character.Id }, _mapper.Map<CharacterDTO>(character));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error updating item.");
			}
		}
		#endregion

		#region Delete
		/// <inheritdoc />
		public async override Task<ActionResult<CharacterDTO>> Delete(int id)
		{
			try
			{
				Character character = await _context.Characters
					.Include(c => c.Movies)
					.SingleOrDefaultAsync(c => c.Id == id);
				if (character == null)
					return NotFound();

				_context.Characters.Remove(character);
				await _context.SaveChangesAsync();

				return _mapper.Map<CharacterDTO>(character);
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error while deleting item.");
			}

		}
		#endregion

		#endregion

		/// <summary>
		/// Maps from a CharacterDTO object to a Character object.
		/// </summary>
		/// <param name="dto"> a CharacterDTO object</param>
		/// <returns>a Character object.</returns>
		private async Task<Character> MapFromDTO(CharacterDTO dto)
		{
			IEnumerable<int> movies = dto.Movies;
			dto.Movies = null;

			Character character = _mapper.Map<Character>(dto);

			character.Movies = await _context.Movies
				.Where(m => movies.Contains(m.Id)).ToListAsync();
			return character;
		}
	}
}
