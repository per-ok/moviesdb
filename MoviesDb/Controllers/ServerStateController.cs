using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb.Model;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MoviesDb.Controllers
{
	public class ServerState
	{
		/// <summary>
		/// Yes. This is where I report my state.  However, if I were not here, how should I be able to report it?
		/// </summary>
		public string State { get; set; }
	}

	[ApiController]
	[Route("/api/v1/[controller]")]
	[Produces(MediaTypeNames.Application.Json)]
	[ApiConventionType(typeof(DefaultApiConventions))]
	public class ServerStateController : ControllerBase
	{
		/// <summary>
		/// Am I up and running? Check me out :-)
		/// </summary>
		/// <returns>My state</returns>
		[HttpGet]
		public ServerState Get()
		{
			ServerState state =
			new()
			{
				State = "Yes, I am up and running.  I don't know about the database though.  You'll have to ask someone else."
			};
			return state;
		}

	}
}
