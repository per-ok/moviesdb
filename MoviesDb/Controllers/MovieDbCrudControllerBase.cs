using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MoviesDb.Model;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MoviesDb.Controllers
{
	/// <summary>
	/// API for CRUD operations.  The intention for this class
	/// is to be an interface between a RESTful API and a database.
	/// </summary>
	/// <typeparam name="TDto"> is the data transfer object type for the
	/// communication.</typeparam>
	[ApiController]
	[Route("/api/v1/[controller]")]
	[Produces(MediaTypeNames.Application.Json)]
	[Consumes(MediaTypeNames.Application.Json)]
	[ApiConventionType(typeof(DefaultApiConventions))]
	public class MovieDbCrudControllerBase<TDto> : ControllerBase
	{
		// Generalised to the necessary level.
		// It is possible to generalise more, but that still would require
		// a lot of work (detecting generalisable code and writing the code
		// for the inheriting classes). Cut and paste + adaptions
		// is faster. :-)

		protected readonly MovieDbContext _context;
		protected readonly IMapper _mapper;

		/// <summary>
		/// The constructor.
		/// </summary>
		/// <param name="context">The database context.</param>
		/// <param name="mapper">The mapper.</param>
		public MovieDbCrudControllerBase(MovieDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		#region Create
		/// <summary>
		/// Create a new element in the database.
		/// </summary>
		/// <param name="dto">The element to put in the database.</param>
		/// <returns>the new database element.</returns>
		[HttpPost]
		public virtual async Task<ActionResult<TDto>> Post([FromBody] TDto dto)
		{
			await Task.CompletedTask;
			throw new System.NotImplementedException();
		}
		#endregion

		#region Read
		/// <summary>
		/// Reads all elements from the database.
		/// </summary>
		/// <returns>Returns all of the database element.</returns>
		[HttpGet]
		public virtual async Task<ActionResult<IEnumerable<TDto>>> Get()
		{
			await Task.CompletedTask;
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Reads one element from the database.
		/// </summary>
		/// <param name="id">The element ID.</param>
		/// <returns>the requested element.</returns>
		[HttpGet("{id}")]
		public virtual async Task<ActionResult<TDto>> Get(int id)
		{
			await Task.CompletedTask;
			throw new System.NotImplementedException();
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates one element in the database.
		/// </summary>
		/// <param name="id">The element ID. (Must be included in the DTO as
		/// well.)</param>
		/// <param name="dto">The <typeparamref name="TDto"/> representation
		/// of the element to update.</param>
		/// <returns>a <typeparamref name="TDto"/> representation of the
		/// updated element.</returns>
		[HttpPut("{id}")]
		public virtual async Task<ActionResult<TDto>> Put(int id, [FromBody] TDto dto)
		{
			await Task.CompletedTask;
			throw new System.NotImplementedException();
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes the identified element.
		/// </summary>
		/// <param name="id">The ID of the element to delete.</param>
		/// <returns>the deleted element.</returns>
		[HttpDelete("{id}")]
		public virtual async Task<ActionResult<TDto>> Delete(int id)
		{
			await Task.CompletedTask;
			throw new System.NotImplementedException();
		}
		#endregion
	}
}