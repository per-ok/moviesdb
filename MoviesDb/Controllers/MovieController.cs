using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb.Model;
using MoviesDb.Model.Domain;
using MoviesDb.Model.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesDb.Controllers
{
	/// <inheritdoc />>
	public class MovieController : MovieDbCrudControllerBase<MoviesDb.Model.DTO.MovieDTO>
	{
		/// <inheritdoc />>
		public MovieController(MovieDbContext context, IMapper mapper) : base(context, mapper)
		{
		}

		#region Create
		/// <inheritdoc />>
		public override async Task<ActionResult<MovieDTO>> Post([FromBody] MovieDTO dto)
		{
			try
			{
				dto.Id = 0;
				Movie movie = await MapFromDTO(dto);

				_context.Movies.Add(movie);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get),
					new { id = movie.Id },
					_mapper.Map<MovieDTO>(movie));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error creating new item.");
			}
		}
		#endregion

		#region Read
		/// <inheritdoc />>
		public override async Task<ActionResult<IEnumerable<MovieDTO>>> Get()
		{
			try
			{
				return _mapper.Map<List<MovieDTO>>(
					await _context.Movies
						.Include(m => m.Characters)
						.Include(m => m.Franchise)
						.ToListAsync()
				);
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}

		/// <inheritdoc />>
		public override async Task<ActionResult<MovieDTO>> Get(int id)
		{
			try
			{
				var movie = _mapper.Map<MovieDTO>(
					await _context.Movies
						.Include(m => m.Characters)
						.Include(m => m.Franchise)
						.SingleOrDefaultAsync(m => m.Id == id));

				if (movie == null)
					return NotFound();

				return movie;
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error retrieving data.");
			}
		}
		#endregion

		#region Update
		/// <inheritdoc />>
		public override async Task<ActionResult<MovieDTO>> Put(int id, [FromBody] MovieDTO dto)
		{
			try
			{
				if (id != dto.Id)
					return BadRequest("Mismatching IDs");

				Movie movie = await _context.Movies
					.Include(m => m.Characters)
					.Include(m => m.Franchise)
					.SingleOrDefaultAsync(m => m.Id == id);

				if (movie == null)
					return NotFound($"Item {id} was not found.");

				// To use the Update() method, we cannot simply overwrite
				// the old character with a new one.  The object and the
				// navigation properties be the same instance as the one
				// retrieved from the database.
				//
				// The ShallowCopy method copies all elements from the source
				// object to the destination object.  This replaces (in this
				// case) the original Characters property by the new one provided
				// by MapFromDTO().

				// Preserving the Character object instance (not the content)
				movie.Characters.Clear();
				var tmp = await MapFromDTO(dto);
				tmp.Characters.ToList().ForEach(m => movie.Characters.Add(m));
				tmp.Characters = movie.Characters;
				// Preserving the movie object instance
				ObjectCopying.ShallowCopy(movie, tmp);

				_context.Movies.Update(movie);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(Get),
					new { id = movie.Id }, _mapper.Map<MovieDTO>(movie));
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error updating item.");
			}
		}
		#endregion

		#region Delete
		/// <inheritdoc />>
		public override async Task<ActionResult<MovieDTO>> Delete(int id)
		{
			try
			{
				var movie = _context.Movies
					.Include(m => m.Characters)
					.Include(m => m.Franchise)
					.SingleOrDefault(m => m.Id == id);
				if (movie == null)
					return NotFound($"Item {id} was not found.");

				_context.Movies.Remove(movie);
				await _context.SaveChangesAsync();

				return _mapper.Map<MovieDTO>(movie);
			}
			catch (System.Exception)
			{
				return StatusCode(StatusCodes.Status500InternalServerError,
					"Error while deleting item.");
			}
		}
		#endregion

		/// <summary>
		/// Maps from a MovieDTO object to a Movie object.
		/// </summary>
		/// <param name="dto">a MovieDTO object</param>
		/// <returns>a Movie object.</returns>
		private async Task<Movie> MapFromDTO(MovieDTO dto)
		{
			IEnumerable<int> characterIds = dto.Characters;
			int franchiseId = dto.Franchise;

			Movie movie = _mapper.Map<Movie>(dto);

			movie.Characters = await _context.Characters
				.Where(c => characterIds.Contains(c.Id)).ToListAsync();
			movie.Franchise = await _context.Franchises.FindAsync(franchiseId);

			return movie;
		}

	}
}
